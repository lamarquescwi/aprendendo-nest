import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { IsNomeDeUsuarioUnico } from './is-nome-de-usuario-unico.validator';

export class Usuario {
  id: number;

  @IsNomeDeUsuarioUnico({
    message: 'Nome de usuário precisa ser único',
  })
  @IsNotEmpty({
    message: 'Nome de usuário é obrigatório.',
  })
  @IsString({
    message: 'Nome de usuário precisa ser uma string.',
  })
  nomeDeUsuario: string;

  @IsEmail(
    {},
    {
      message: 'E-mail precisa ser um endereço de email válido.',
    },
  )
  email: string;

  @IsNotEmpty({
    message: 'Senha é obrigatório.',
  })
  senha: string;

  @IsNotEmpty({
    message: 'Nome Completo é Obrigatório.',
  })
  nomeCompleto: string;
  dataDaEntrada: Date;
}
